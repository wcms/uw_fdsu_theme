<?php

/**
 * @file
 */

/**
 * Implements hook_preprocess_HOOK().
 *
 * Add custom color CSS file.
 */
function uw_fdsu_theme_preprocess_html(&$variables) {
  // Add to body element a class to express the faculty or other organizational
  // unit that this page is from. This is used to select the faculty colors in
  // CSS.
  $variables['classes_array'][] = 'org_' . variable_get('uw_fdsu_theme_color_css', 'default');

  // Add JavaScript for users with content authorship roles.
  global $user;
  if (array_intersect($user->roles, array('administrator', 'site manager', 'content editor', 'content author'))) {
    drupal_add_js(drupal_get_path('theme', 'uw_fdsu_theme') . '/scripts/hide-admin.js', 'file');
  }
  // Add javascript for all users.
  drupal_add_js(drupal_get_path('theme', 'uw_fdsu_theme') . '/scripts/expandable.js', 'file');
}

/**
 * Implements theme_breadcrumb().
 */
function uw_fdsu_theme_breadcrumb(array $variables) {
  $breadcrumb = $variables['breadcrumb'];

  // Remove the local site home link, and replace it with a link that uses the
  // site name.
  if (isset($breadcrumb[0])) {
    array_shift($breadcrumb);
    array_unshift($breadcrumb, l(variable_get('site_name', 'Site home'), '<front>'));
  }

  // Return the breadcrumb with separators.
  if (!empty($breadcrumb)) {
    return '<nav aria-label="Breadcrumb" class="breadcrumb"><ol><li>' . implode('</li><li>', $breadcrumb) . '</li></ol></nav>';
  }
}

/**
 * Add theme suggestion for all content types.
 */
function uw_fdsu_theme_process_page(&$variables) {
  if (isset($variables['node'])) {
    if ($variables['node']->type != '') {
      $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
    }
  }
}

/**
 * Separate out the <h1> if it is part of the content.
 *
 * The <h1> is part of the content in uw_ct_event.
 */
function uw_fdsu_theme_separate_h1_and_content($page, $title) {
  $content = render($page['content']);
  // Avoid the regex engine when not needed.
  if (strpos($content, '<h1>') !== FALSE && preg_match(',^(.*)<h1>(.*)</h1>(.*)$,ms', $content, $matches)) {
    $title = $matches[2];
    $content = $matches[1] . $matches[3];
  }
  return array($title, $content);
}
