<?php

/**
 * @file
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function uw_fdsu_theme_form_system_theme_settings_alter(&$form, $form_state) {

  $form['color'] = array(
    '#type' => 'fieldset',
    '#title' => t('Color scheme'),
  );

  $form['color']['css'] = array(
    '#type' => 'select',
    '#title' => t('Select a color scheme to use'),
    '#default_value' => variable_get('uw_fdsu_theme_color_css', 'default'),
    '#options' => array(
      'default' => t('Default (yellow/red)'),

      'art' => t('Arts (orange)'),
      'eng' => t('Engineering (purple)'),
      'env' => t('Environment (green)'),
      'ahs' => t('Health (teal)'),
      'mat' => t('Mathematics (pink)'),
      'sci' => t('Science (blue)'),

      'cgc' => t('Conrad Grebel University College'),
      'ren' => t('Renison University College'),
      'stj' => t('St. Jerome’s University'),
      'stp' => t('St. Paul’s University College'),

      'school' => t('School (red)'),
    ),
  );

  $form['google_analytics'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Analytics'),
  );
  $form['google_analytics']['toggle_google_analytics'] = array(
    '#type' => 'checkbox',
    '#title' => t('Google Analytics'),
    '#default_value' => variable_get('google_analytics_enable', 1),
    '#description' => t("Enable tracking this site using the university's global Google Analytics code. This should be disabled for sites that are not meant to be public (e.g. Canary)."),
  );

  $form['#submit'][] = 'uw_fdsu_theme_set_custom_variable';

}

/**
 *
 */
function uw_fdsu_theme_set_custom_variable($form, &$form_state) {

  if (isset($form_state['values']['css'])) {
    variable_set('uw_fdsu_theme_color_css', $form_state['values']['css']);
  }
  if (isset($form_state['values']['toggle_google_analytics'])) {
    variable_set('google_analytics_enable', $form_state['values']['toggle_google_analytics']);
  }
}
