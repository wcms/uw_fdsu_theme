/**
 * @file
 */

(function ($) {
  $(document).ready(function () {

    // Adds a toggle for hiding admin/site-management related elements.
    $toggler = $('<button>')
    // plus/minus symbol.
      .text('\u00b1')
      .attr('title', 'Show/hide revision elements')
      .css({
        'position': 'fixed',
        'padding': '0',
        'bottom': '40px',
        'right': '10px',
        'cursor': 'pointer',
        'font-size': '2em',
        'width': '1em',
        'color': '#96172e',
        'background-color' : '#fff',
        'text-decoration': 'none'
        })
        .prependTo($('body'))
        .click(function (e) {
          $('#main .node-tabs, #block-workbench-block, #main .editlink').toggle();
        });

  });
}(jQuery));
