/**
 * @file
 */

(function ($) {
  $(document).ready(function () {

    // Enables expandable/collapsable regions
    // .node-type-uw-web-page.
  
    // ISTWCMS-3112: Text-select solution.
    function isSelected() {
      if (window.getSelection) { return window.getSelection().toString().length > 0}        // https://developer.mozilla.org/en-US/docs/Web/API/Window/getSelection#Browser_compatibility 
      else if (document.selection) {return document.selection.createRange().text.length > 0}// IE < 9
      else return false;
    }
    $('#content .field-item > .expandable > h2:first-child').click(function () {
        if (!isSelected()) {
          $expandable = $(this).parent();
          if ($expandable.hasClass('expanded')) {
            $expandable.removeClass('expanded');
            // ISTWCMS-2178: Ensure that aria-expanded displays correctly.
            $expandable.find('div[role="button"]').attr('aria-expanded', 'false');
            $('.expandable-content',$expandable).prev().addClass('last-visible');
          }
          else {
            $expandable.addClass('expanded');
            // ISTWCMS-2178: Ensure that aria-expanded displays correctly.
            $expandable.find('div[role="button"]').attr('aria-expanded', 'true');
            $('.expandable-content',$expandable).prev().removeClass('last-visible');
          }  
        }
    }).wrapInner('<div role="button" tabindex="0">');

    // ISTWCMS-2176: Adding aria-expanded set to false to all the buttons.
    $('#content .field-item > .expandable').each(function () {
      $(this).find('div[role="button"]').attr('aria-expanded', 'false');
    });

    // If the div[role="button"] gets focus and key 'enter' is pressed.
    $(function(){
      $('#content .field-item > .expandable > h2:first-child > div[role="button"]').keypress(function (e) {
        $expandable = $(this).parent().parent();
        if (e.keyCode == 13) {
          if ($expandable.hasClass('expanded')) {
            // ISTWCMS-2178: Ensure that aria-expanded displays correctly.
            $expandable.removeClass('expanded');
            $(this).attr('aria-expanded', 'false');
            $('.expandable-content',$expandable).prev().addClass('last-visible');
          }
          else {
            $expandable.addClass('expanded');
            // ISTWCMS-2178: Ensure that aria-expanded displays correctly.
            $(this).attr('aria-expanded', 'true');
            $('.expandable-content',$expandable).prev().removeClass('last-visible');
          }
        }
      });
    });

    // Add last-visible class to the last visible item before the hidden content, and the last item in the hidden content, to remove lower margins
    // .node-type-uw-web-page.
    $('#content .field-item > .expandable > .expandable-content').prev().addClass('last-visible');
    // .node-type-uw-web-page.
    $('#content .field-item > .expandable > .expandable-content :last-child').addClass('last-visible');
    // If there is more than one expandable region on the page, add expand/collapse all functions.
    if ($('#content .field-item > .expandable').length > 1) {
      // .node-type-uw-web-page.
      $('#content .field-item > .expandable:first').before('<div class="expandable-controls"><button class="expand-all">Expand all</button><button class="collapse-all">Collapse all</button></div>');
      $('#content .expandable-controls .expand-all').click(function () {
        // Rather than recreate clicking logic here, just click the affected items.
        $('#content .expandable:not(.expanded) h2:first-child').click();
      });
      $('#content .expandable-controls .collapse-all').click(function () {
        // Rather than recreate clicking logic here, just click the affected items.
        $('#content .expandable.expanded h2:first-child').click();
      });
    }

    // Do things when the anchor changes.
    window.onhashchange = uw_fdsu_anchors;
    // Trigger the event on page load in case there is already an anchor.
    // Navbar seems to bounce the page around on page load and I couldn't find a way to detect when it finished...so we wait before we trigger.
    setTimeout(uw_fdsu_anchors, 1750);

    // Function to do things when the anchor changes.
    function uw_fdsu_anchors() {
      if (location.hash) {
        // Check if there is an ID with this name.
        if ($(location.hash).length) {
          // If it's in an unexpanded expandable content area, expand the expandable content area.
          $(location.hash,'.expandable:not(.expanded)').closest('.expandable').find('h2:first-child').click();
          // Get the toolbar/navbar height.
          if ($('#toolbar').length) {
            $barheight = $('#toolbar').height();
          }
          else {
            $barheight = $('#navbar-bar').height();
            if ($('.navbar-tray-horizontal.navbar-active').length) {
              $barheight += $('.navbar-tray-horizontal.navbar-active').height();
            }
          }

          // Scroll to the ID, taking into account the toolbar if it exists
          // "html" works in Firefox, "body" works in Chrome/Safari.
          $('html, body').scrollTop($(location.hash).offset().top - $barheight);
        }
      }
    }

  });
}(jQuery));

